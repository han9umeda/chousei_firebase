import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyDw1FbH24ZffcuoXJQ8l21jI7FOjsBcoxE",
  authDomain: "chousei-firebase-4b5d8.firebaseapp.com",
  databaseURL: "https://chousei-firebase-4b5d8-default-rtdb.firebaseio.com",
  projectId: "chousei-firebase-4b5d8",
  storageBucket: "chousei-firebase-4b5d8.appspot.com",
  messagingSenderId: "775639700099",
  appId: "1:775639700099:web:8601b5d8ed6b8deb7841b6"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
